/**
 * @description
 * Adds computed property run with run details
 * Also adds mounted hook that loads run info if not present
 * @mixin
 */
export default {
	computed: {
		run() {
			const id = this.$route.params.id;
			return this.$store.getters.run(id);
		},
	},
	mounted() {
		if (!this.run)
			this.$store.dispatch('getRun', { id: this.$route.params.id });
	}
}