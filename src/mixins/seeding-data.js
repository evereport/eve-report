const SEEDING_STATUS = {
	0: "Not started",
	1: "In progress",
	2: "Failed",
	3: "Succeeded",
};

export default {
	computed: {
		mode() {
			return 1;
		},
		behaviour() {
			return 1;
		},
		seeding_status() {
			return SEEDING_STATUS[this.seeder.seeded]
		}
	},
	methods: {

	}
}