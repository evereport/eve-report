/**
 * @description
 * Allowed error type
 * @type {string}
 */
const ALLOWED_ERROR = 'MI_NOT_PASSED';
/**
 * @description
 * Adds couple of computed properties
 * @property {object} computed
 * @property {object|null} computed.error - Run error
 * @property {boolean} computed.passed - Run passed
 * @property {boolean} computed.broken - If run error was fatal to runner execution
 * @property {string} computed.created - Human readable date interval from run execution end
 * @property {object} computed.config - Runner config for this run
 * @mixin
 */
export default {
	computed: {
		error() {
			return (this.run.error) ? JSON.parse(this.run.error) : null;
		},
		passed() {
			return !(this.error);
		},
		broken() {
			return this.error && this.error.name && this.error.name !== ALLOWED_ERROR && this.run.fatal;
		},
		created() {
			return this.$moment(this.run.created).fromNow();
		},
		config() {
			return (this.run.config) ? JSON.parse(this.run.config) : {};
		},
	}
}