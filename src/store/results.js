const instances = require('./axios');
/**
 * @description
 * Vuex module for operating test runs results
 * @type Vuex
 */
module.exports = {
	state: {
		/**
		 * @description
		 * Stores run results
		 * @type {Array.<object>}
		 */
		runs: [],
		/**
		 * @description
		 * Stores complete run details
		 * @type {object}
		 */
		full: {},
		/**
		 * @description
		 * Shows whether runs were requested at least once
		 * @type {boolean}
		 */
		initialized: false,
	},
	getters: {
		initialized: state => state.initialized,
		runs: state => state.runs,
		run: state => id => state.full[id],
		test: state => (runId, testId) => (state.full[runId]) ? state.full[runId].__tests[testId] : undefined
	},
	mutations: {
		/**
		 * @description
		 * Stores run results
		 * @param {object} state - Current vuex state
		 * @param {Array.<object>} payload - Run results to add
		 */
		addRuns(state, payload) {
			state.initialized = true;
			state.runs.push(...payload);
		},
		/**
		 * @description
		 * Stores run results
		 * @param {object} state - Current vuex state
		 * @param {object} payload - Run details
		 */
		addRun(state, payload) {
			const run = { [payload.id]: payload };

			const grep = function (suite) {
				return {
					...(suite.suites || []).reduce((o, s) => Object.assign(o, grep(s)), { [suite.id]: suite }),
					...(suite.tests || []).reduce((o, t) => { o[t.id] = t; return o}, {})
				};
			};

			const tree = function(suite, depth = 0) {
				suite.depth = depth;
				suite.suite = true;
				return {
					id: suite.id,
					depth: depth,
					tests: suite.tests.map(t => { t.depth = depth + 1; return t.id }),
					suites: (suite.suites || []).map(s => tree(s, depth + 1))
				}
			};

			const suitesMap = payload.suites.map(s => s.suite);

			run[payload.id].__tests = suitesMap.map(grep).reduce((o, s) => Object.assign(o, s), {});
			run[payload.id].__tree = suitesMap.map(tree);
			console.log(payload);
			//It's kinda reactive setter
			state.full = { ...state.full, ...run};
		}
	},
	actions: {
		/**
		 * @description
		 * Gets run results from server. With pagination.
		 * @param {object} context - Vuex action context
		 * @param {function} context.commit - Commit function
		 * @param {object} payload - Payload
		 * @param {number} payload.limit - Rows limit
		 * @param {number} payload.offset - Rows offset
		 */
		getRuns({ commit }, { limit = 10, offset = 0 } = {}) {
			instances.get.runs.request({
				params: {
					limit,
					offset
				}
			}).then((result) => commit('addRuns', result.data))
				.catch(console.log); //TODO not console
		},
		/**
		 * @description
		 * Gets run results from server. With pagination.
		 * @param {object} context - Vuex action context
		 * @param {function} context.commit - Commit function
		 * @param {object} payload - Payload,
		 * @param {string} payload.id - UUIDv4 run id
		 */
		getRun({ commit }, { id } = {}) {
			instances.get.run.request({
				url: `/run/${id}`
			}).then(result => commit('addRun', result.data))
				.catch(console.log);
		}
	}

};