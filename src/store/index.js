import Vue from "vue";
import Vuex from "vuex";
import results from './results';
import filters from "./filters";

Vue.use(Vuex);

export default new Vuex.Store({
  state: {},
  mutations: {},
  actions: {},
  modules: {
    results,
    filters
  }
});
