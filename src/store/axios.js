const qs = require('qs');
const axios = require('axios');
const config = require('../config');
/**
 * List of pre-created axios instances
 * @type {object}
 */
module.exports = {
	/**
	 * @description
	 * Getters
	 */
	get: {
		/**
		 * @description
		 * Returns list of runs
		 */
		runs: axios.create({
			baseURL: config.server.url,
			url: '/run',
			method: 'get',
			paramsSerializer: params => qs.stringify(params)
		}),
		/**
		 * @description
		 * Returns full record with dependencies of a single run by it's id
		 */
		run: axios.create({
			baseURL: config.server.url,
			method: 'get',
		})
	}
};