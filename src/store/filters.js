const rq = (state, id) => {
	const suite = state.expanded.find(e => e.id === id);
	if (!suite) return false;
	return (!suite.parentId) ? true : rq(state, suite.parentId);
};

export default {
	state: {
		states: {
			0: {
				name: "Passed",
				color: 'green'
			},
			1: {
				name: 'Failed',
				color: 'red'
			},
			2: {
				name: 'Pending',
				color: 'blue'
			},
			3: {
				name: 'Broken',
				color: 'orange'
			},
		},
		enum: {
			PASSED: 0,
			FAILED: 1,
			PENDING: 2,
			BROKEN: 3
		},
		active: {
			0: true,
			1: true,
			2: true,
			3: true
		},
		expanded: []
	},
	getters: {
		states: (state) => state.states,
		isActive: (state) => (id) => state.active[id],
		enum: (state) => (name) => state.states[state.enum[name]],
		enumIsActive: (state) => (name) => state.active[state.enum[name]],
		//
		isExpanded: (state) => (id) => rq(state, id)
	},
	mutations: {
		toggle: (state, id) => state.active[id] = !state.active[id],
		activate: (state, id) => state.active[id] = true,
		deactivate: (state, id) => state.active[id] = false,
		all: (state) => {
			for (let id of Object.keys(state.active))
				state.active[id] = true;
		},
		none: (state) => {
			for (let id of Object.keys(state.active))
				state.active[id] = false;
		},
		//
		toggleSuite: (state, payload) => {
			const index = state.expanded.findIndex(e => e.id === payload.id);
			if (index !== -1)
				state.expanded.splice(index, 1);
			else
				state.expanded.push(payload);
		}

	}
}