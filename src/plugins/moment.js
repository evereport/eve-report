import Vue from 'vue';
import * as moment from "moment";

const Moment = class VueMoment {
	/**
	 * @description
	 * moment.js function
	 * @type {*}
	 */
	static moment = moment;
	static install(Vue) {
		Vue.prototype.$moment = this.moment;
	}
};

Vue.use(Moment);

export default Moment;