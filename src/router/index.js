import Vue from "vue";
import VueRouter from "vue-router";
import Overview from "../components/views/Overview";
import RunDetails from "../components/views/RunDetails";
import TestDetails from "../components/run/TestDetails";
import Details from "../components/views/Details";

Vue.use(VueRouter);

const routes = [
  { path: '/', component: Overview },
  { path: '/run/:id', component: Details },
  {
    path: '/run/:id/tree',
    component: RunDetails,
    children: [
      {
        path: '',
        components: {
          test: TestDetails
        }
      },
      {
        path: 'test/:testId',
        components: {
          test: TestDetails
        }
      }
    ]
  },
];

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes
});

export default router;
